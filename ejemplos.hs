doubleMe x = x + x 

--doubleUs x y = x*2 + y*2

--Suma
doubleUs x y = doubleMe x + doubleMe y

doubleSmallNumber x = if x > 100
                        then x
                        else x*2

--Ejemplo funciones con if
doubleSmallNumber' x = (if x > 100 then x else x*2) + 1

--Despliega la expresión ""¡Soy yo, Conan O'Brien!""
conanO'Brien = "¡Soy yo, Conan O'Brien!"

--Si x es menor a 10 cambia el numero por un "Boom!" si no cambialo por un "Bang!", solo numeros pares
boomBangs xs = [if x < 10 then "Boom!" else "bang" | x <- xs, odd x] 

--Funcion propia del tamaño de una lista
length' xs = sum [1 | _ <- xs]

--Todos los elementos de la lista ns seran multiplicados por 2
numPares ns = [x*2 | x <- ns]

--Punto extra-------------------------------------------------------------------------------------------------
uno = [1]

dos = [1,2]

tres = [1,2,3]

cuatro = [1,2,3,4]

--Lista de listas
list = [uno, uno, tres, dos, tres, cuatro, dos, uno]

--Ingresa una lista l donde sus elementos son listas. Devuelve una lista de listas solo de tamaño n . 
listasDeN l n = [xs | xs <- l, length xs == n]

--Devuelve true si n es un número par, en otro caso devuelve false.
esPar :: Int -> Bool
esPar x = if mod x 2 == 0 then True else False

--Concatena listas de l que el tamaño de sus elementos sean pares.
dobleListaComp l = [ xs++xs | xs <- l, (length xs) `mod` 2 == 0]

--Punto extra-------------------------------------------------------------

--Ajusto de patrones----------------------------------------
--Despliega texto "¡El siete de la suerte!" al ingresar como argumento 7, en otro caso "Lo siento, ¡no es tu día de suerte!".
lucky :: (Integral a) => a -> String
lucky 7 = "¡El siete de la suerte!"
lucky x = "Lo siento, ¡no es tu día de suerte!"

--Si se ingresa un número de 1 al 7, se despliega el String que representa al número seleccionado. En otro caso despliega "No entre 1 y 5". 
sayMe :: (Integral a) => a -> String
sayMe 1 = "¡Uno!"
sayMe 2 = "¡Dos!"
sayMe 3 = "¡Tres!"
sayMe 4 = "¡Cuatro!"
sayMe 5 = "¡Cinco!"
sayMe x = "No entre 1 y 5"

--Funcion recursiva de un factorial.
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n-1)

--Funcion que trabaja con Char
charName :: Char -> String
charName 'a' = "Albert"
charName 'b' = "Broseph"
charName 'c' = "Cecil"

--Función que tomara dos vectores 2D (representados con duplas) y que devolviera la suma de ambos? Para sumar dos vectores sumamos primero sus componentes x y sus componentes y de forma separada
addVectors :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors a b = (fst a + fst b, snd a + snd b)

addVectors' :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors' (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

--Propia funcion head
head' :: [a] -> a
head' [] = error "No puedes utilizar head con listas vacias"
head' (x:_) = x

--Función trivial en el que dado un nombre y un apellido devuelva sus iniciales.
initials :: [Char] -> [Char] -> String
initials firstname lastname = (head firstname) ++ ". " ++ (head lastname) ++ "."